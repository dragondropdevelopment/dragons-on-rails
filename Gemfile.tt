source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "<%= RUBY_VERSION %>"

gem "bcrypt"<%= gemfile_requirement("bcrypt") %>
gem "bootsnap"<%= gemfile_requirement("bootsnap") %>, require: false
gem "coffee-rails"<%= gemfile_requirement("coffee-rails") %>
gem "pg"<%= gemfile_requirement("pg") %>
gem "puma", "~> 4.1"
gem "rails", "~> <%= Rails.version %>"
gem "sass-rails", "~> 6.0"
gem "sidekiq"
<% if gemfile_requirement("turbolinks") -%>
gem "turbolinks"<%= gemfile_requirement("turbolinks") %>
<% end -%>
gem "webpacker"<%= gemfile_requirement("webpacker") %>

group :development, :test do
  gem "byebug"
end

group :development do
  gem "brakeman", require: false
  gem "dotenv-rails"
  gem "listen"<%= gemfile_requirement("listen") %>
  gem "pry-rails" # A much better console than IRB
  gem "rubocop-rails", require: false
  gem "spring"
<% if gemfile_requirement("spring-watcher-listen") -%>
  gem "spring-watcher-listen"<%= gemfile_requirement("spring-watcher-listen") %>
<% end -%>
end

group :test do
  gem "capybara"<%= gemfile_requirement("capybara") %>
  gem "chromedriver-helper"
  gem "cucumber-rails", require: false
  gem "database_cleaner"
  gem "factory_bot_rails"
  gem "launchy"
  gem "rspec-rails"
  gem "simplecov", require: false
end
